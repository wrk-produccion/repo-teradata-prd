-- -------------------------------------------------------------------------------------------------------------------------
-- RESUMEN
-- Sistema                 	: RCC
-- Descripcion             	: Creacion de vista V_VPC_FACT_SALDOS_RCC_AWS
-- Fecha de Creacion       	: 06/05/2021
-- Iniciativa              	: VPC
-- Tabla DESTINO           	: - 
--							  -
-- Tabla FUENTES			: - 
--							: -
-- Parametros               : -
-- Proceso completado		: -
-- Observacion				: Sin Observaciones
-- -------------------------------------------------------------------------------------------------------------------------
-- Modificaciones
-- Motivo         Fecha             Nombre               Descripcion
-- -------------------------------------------------------------------------------------------------------------------------
-- 0.0            06/05/2021        Jose R.              Version Inicial
-- -------------------------------------------------------------------------------------------------------------------------
-- Tener en cuenta que si es un objeto nuevo va con CREATE
CREATE VIEW E_DW_VIEWS.V_VPC_FACT_SALDOS_RCC_AWS
(
	Periodo_Val
	,Fecha_SBS_Dt
	,Party_SBS_Id
	,Cod_SBS_Val
	,Party_Persona_Id 
	,Tipo_Documento_Id
	,Tipo_Documento_Cd
	,Tipo_Documento_Desc
	,Numero_Documento_Val
	,Party_CUC_Id
	,Cod_Unico_Val
	,Empresa_Cd
	,Cuenta_RCC_Val      
	,Tipo_Credito_Cd
	,Condicion_Val              
	,Clasificacion_Deudor_Cd
	,Saldo_Amt
)
AS
LOCKING ROW ACCESS
SELECT
	BASE.Periodo_Val
	,BASE.Fecha_SBS_Dt
	,BASE.Party_SBS_Id
	,BASE.Cod_SBS_Val
	,BASE.Party_Persona_Id 
	,BASE.Tipo_Documento_Id
	,BASE.Tipo_Documento_Cd
	,BASE.Tipo_Documento_Desc	
	,CASE 
		WHEN ENDH.Key_Value IS NULL THEN 'SV' 
		ELSE ENDH.Key_Value
	END AS Numero_Documento_Val
	,BASE.Party_CUC_Id
	,BASE.Cod_Unico_Val
	,BASE.Empresa_Cd
	,BASE.Cuenta_RCC_Val      
	,BASE.Tipo_Credito_Cd
	,BASE.Condicion_Val              
	,BASE.Clasificacion_Deudor_Cd
	,BASE.Saldo_Amt
FROM E_DW_VIEWS.V_VPC_FACT_SALDOS_RCC BASE
LEFT JOIN E_DW_VIEWS.V_MST_ENCRIP_NRO_DOC_MDL_HISTDIA ENDH
	ON BASE.Numero_Documento_Val = ENDH.Nro_Doc;