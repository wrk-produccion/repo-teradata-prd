--Para Instalador
SELECT 1 FROM DBC.TABLESV WHERE DATABASENAME = 'D_DW_VIEWS.' AND TABLENAME = 'V_VPC_SEGMENTACION_MERC';
.IF ERRORCODE <> 0 THEN .GOTO ERRORFOUND;
.IF ACTIVITYCOUNT = 0 THEN .GOTO NEXT
DROP VIEW D_DW_VIEWS.V_VPC_SEGMENTACION_MERC;
.IF ERRORCODE <> 0 THEN .GOTO ERRORFOUND;
.LABEL NEXT

-- -------------------------------------------------------------------------------------------------------------------------
-- RESUMEN
-- Sistema                 	: CONFIGURADOR SECTORISTA MERCADO VPC
-- Descripcion             	: Creacion de vista V_VPC_SEGMENTACION_MERC
-- Fecha de Creacion       	: 22/12/2020
-- Iniciativa              	: VPC
-- Tabla DESTINO           	: - 
--							  -
-- Tabla FUENTES			: - 
--							: -
-- Parametros               : -
-- Proceso completado		: -
-- Observacion				: Sin Observaciones
-- -------------------------------------------------------------------------------------------------------------------------
-- Modificaciones
-- Motivo         Fecha             Nombre               Descripcion
-- -------------------------------------------------------------------------------------------------------------------------
-- 0.0            22/12/2020        David Silva C              Version Inicial
-- -------------------------------------------------------------------------------------------------------------------------
CREATE VIEW D_DW_VIEWS.V_VPC_SEGMENTACION_MERC
(
 Fecha_SBS_Dt
,Cod_SBS_Val
,Tipo_SBS_Unico
,Cod_SBS_Unico
,Persona_Id
,Cod_Tipo_Documento
,Nro_Documento
,Cliente_Id
,Cod_Unico_Val
,CIIU_Cd
,SEI_Mercado_Id
,Estado_Contribuyente_Val
,Ubigeo_Cd
,Region_Val
,Cod_Sectorista_Val
,Region_Sect_Val
,Flg_Cartera_NO_G0
,Cod_Segmento_Merc_Val
,Monto_Facturacion_Mnt
,Monto_Deuda_Agr_Mnt
,Monto_Deuda_SF_Mnt
,Entidad_Cnt
,Monto_Deuda_PROM_Mnt
,Banca_Mes_Val
,Segmento_Mes_Val
,Logica_Banca_Mes_Val
,Provincia_Mes_Val
,Departamento_Mes_Val
,Region_Mes_Val
,Logica_region_Mes_Val
,Provincia_Fin_Val
,Departamento_Fin_Val
,Region_Fin_Val
,Logica_region_Fin_Val
,Periodo_Ini_Val
,Banca_Fin_Val
,Segmento_Fin_Val
,Logica_Banca_Fin_Val

)
AS
LOCKING ROW ACCESS
SELECT
 Fecha_SBS_Dt
,Cod_SBS_Val
,Tipo_SBS_Unico
,Cod_SBS_Unico
,Persona_Id
,Cod_Tipo_Documento
,Nro_Documento
,Cliente_Id
,Cod_Unico_Val
,CIIU_Cd
,SEI_Mercado_Id
,Estado_Contribuyente_Val
,Ubigeo_Cd
,Region_Val
,Cod_Sectorista_Val
,Region_Sect_Val
,Flg_Cartera_NO_G0
,Cod_Segmento_Merc_Val
,Monto_Facturacion_Mnt
,Monto_Deuda_Agr_Mnt
,Monto_Deuda_SF_Mnt
,Entidad_Cnt
,Monto_Deuda_PROM_Mnt
,Banca_Mes_Val
,Segmento_Mes_Val
,Logica_Banca_Mes_Val
,Provincia_Mes_Val
,Departamento_Mes_Val
,Region_Mes_Val
,Logica_region_Mes_Val
,Provincia_Fin_Val
,Departamento_Fin_Val
,Region_Fin_Val
,Logica_region_Fin_Val
,Periodo_Ini_Val
,Banca_Fin_Val
,Segmento_Fin_Val
,Logica_Banca_Fin_Val
FROM D_DW_EXPLO_TABLES.REL_VPC_SEGMENTACION_MERC;
