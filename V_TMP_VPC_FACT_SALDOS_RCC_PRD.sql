--se agrega cambio en vista ante obs de QA
--20210705_2056: se anhade campo
-- -------------------------------------------------------------------------------------------------------------------------
-- RESUMEN
-- Sistema                 	: RCC
-- Descripcion             	: Creacion de vista V_TMP_VPC_FACT_SALDOS_RCC
-- Fecha de Creacion       	: 10/06/2021
-- Iniciativa              	: VPC
-- Tabla DESTINO           	: - 
--							  -
-- Tabla FUENTES			: - 
--							: -
-- Parametros               : -
-- Proceso completado		: -
-- Observacion				: Sin Observaciones
-- -------------------------------------------------------------------------------------------------------------------------
-- Modificaciones
-- Motivo         Fecha             Nombre               Descripcion
-- -------------------------------------------------------------------------------------------------------------------------
-- 0.0            10/06/2021        Jose R.              Version Inicial
-- -------------------------------------------------------------------------------------------------------------------------
-- Tener en cuenta que si es un objeto nuevo va con CREATE
CREATE VIEW E_DW_STAGING_CLOUD.V_TMP_VPC_FACT_SALDOS_RCC
(
	Periodo_Val
	,Fecha_SBS_Dt
	,Party_SBS_Id
	,Cod_SBS_Val	
	,Persona_Id
	,Tipo_Documento_Cd
	,Tipo_Documento_Desc
	,Numero_Documento_Val
	,Cliente_Id
	,Cod_Unico_Val	
	,Empresa_Cd
	,NomEmpresaFinanc_Desc
	,NomCortoEmpresaFinanc_Desc
	,TipoEmpresaFinanc_Id
	,TipoEmpresaFinanc_Desc
	,EstadoEmpresaFinanc_Id
	,EstadoEmpresaFinanc_Desc	
	,Cuenta_RCC_Val
	,Tipo_Credito_Cd
	,Tipo_Credito_Desc
	,Condicion_Val	
	,Clasificacion_Deudor_Cd	
 	,Tipo_Producto_RCC_Cd
 	,Tipo_Producto_RCC_Desc	
	,Producto_RCC_Cd
	,Producto_RCC_Desc
	,Sub_Producto_RCC_Cd
	,Sub_Producto_RCC_Desc
 	,Situacion_RCC_Cd
 	,Situacion_RCC_Desc		
	,Saldo_Amt
	,Flg_Lin_Mercado
)
AS
LOCKING ROW ACCESS
SELECT 
	FRRC.Fch_SBS/100+190000 AS Periodo_Val
	,FRRC.Fch_SBS AS Fecha_SBS_Dt
	,PSBS.Party_Id AS Party_SBS_Id
	,FRRC.Codigo_SBS AS Cod_SBS_Val	
	,COALESCE(SMRC.Persona_Id, PPER.Party_Id) AS Persona_Id
	,COALESCE(SMRC.Cod_Tipo_Documento, FRRC.Cod_Tipo_Documento) AS Tipo_Documento_Cd		
	,COALESCE(PITY.Party_Identification_Type_Desc, PPER.Party_Identification_Type_Desc) AS Tipo_Documento_Desc
	,COALESCE(ENCR.Key_Value, PPER.Key_Value) AS Numero_Documento_Val
	,COALESCE(SMRC.Cliente_Id, DICL.Cliente_Id) AS Cliente_Id
	,COALESCE(SMRC.Cod_Unico_Val, DICL.CUC_Num) AS Cod_Unico_Val		
	,FRRC.Cod_Instit_Financiera AS Empresa_Cd
	,EMPR.NomEmpresaFinanc_Desc AS NomEmpresaFinanc_Desc
	,EMPR.NomCortoEmpresaFinanc_Desc AS NomCortoEmpresaFinanc_Desc
	,EMPR.TipoEmpresaFinanc_Id AS TipoEmpresaFinanc_Id
	,DTEM.TipoEmpresaFinanc_Desc AS TipoEmpresaFinanc_Desc
	,EMPR.EstadoEmpresaFinanc_Id AS EstadoEmpresaFinanc_Id
	,DEEM.EstadoEmpresaFinanc_Desc AS EstadoEmpresaFinanc_Desc	
	,FRRC.Cod_Cuenta_RCC AS Cuenta_RCC_Val
	,FRRC.Tipo_Credito AS Tipo_Credito_Cd
	,DTCR.RTCR_DesTipoCred AS Tipo_Credito_Desc		
	,FRRC.Condicion AS Condicion_Val	
	,FRRC.Cod_Clasificacion_Deudor AS Clasificacion_Deudor_Cd
 	,CTRC.Tipo_Producto_RCC_Cd AS Tipo_Producto_RCC_Cd
 	,CTRC.Tipo_Producto_RCC_Desc AS Tipo_Producto_RCC_Desc	
	,CTRC.Producto_RCC_Cd AS Producto_RCC_Cd
	,CTRC.Producto_RCC_Desc AS Producto_RCC_Desc
	,CTRC.Sub_Producto_RCC_Cd AS Sub_Producto_RCC_Cd
	,CTRC.Sub_Producto_RCC_Desc AS Sub_Producto_RCC_Desc
 	,CTRC.Situacion_RCC_Cd AS Situacion_RCC_Cd
 	,CTRC.Situacion_RCC_Desc AS Situacion_RCC_Desc		
	,FRRC.Saldo AS Saldo_Amt
	,CASE 
		WHEN TPRC.FLG = 1 AND CEMP.FLG = 1 AND TIEM.FLG = 1 AND COALESCE(ENNF.FLG,0) = 0 THEN 'S' 
		ELSE 'N' 
	END AS Flg_Lin_Mercado
-- Base de RCC
FROM E_DW_VIEWS.FACT_REPORTE_RCC FRRC
-- Cuentas Contables VPC
INNER JOIN E_DW_VIEWS.V_DIM_VPC_CTA_CTBLE_RCC CTRC 
	ON FRRC.Cod_Cuenta_RCC = CTRC.Cuenta_Ctble_RCC_Cd
-- Alinear Personas y CUs obtenidos en Mercado
LEFT JOIN E_DW_VIEWS.V_VPC_SEGMENTACION_MERC SMRC
	ON FRRC.Fch_SBS = SMRC.Fecha_SBS_Dt
	AND FRRC.Codigo_SBS = SMRC.Cod_SBS_Val
-- Encriptacion	para informacion obtenida en Mercado
LEFT JOIN E_DW_VIEWS.V_MST_ENCRIP_NRO_DOC_MDL_HISTDIA ENCR
	ON SMRC.Nro_Documento = ENCR.Nro_Doc
	-- Particion minima en tabla de encriptacion
	AND ENCR.CodDia BETWEEN '20160101'(DATE, FORMAT 'yyyymmdd') AND CURRENT_DATE
-- Calculo de Periodos para Instituciones Financieras
LEFT JOIN (
	SELECT 
		(FecInformacion_Dt + 1) FechaIni
		,COALESCE((MAX(FCHE.FecInformacion_Dt) OVER (ORDER BY FCHE.FecInformacion_Dt ROWS BETWEEN 1 FOLLOWING AND 1 FOLLOWING)), CAST('20991231' AS DATE FORMAT 'YYYYMMDD')) FechaFin
	FROM (
		SELECT 
			FecInformacion_Dt
		FROM E_DW_VIEWS.V_CONFIG_EMPR_FINANC_VPC_HST
		GROUP BY
			FecInformacion_Dt
	) FCHE
) PEEM
	ON FRRC.Fch_SBS BETWEEN PEEM.FechaIni AND PEEM.FechaFin
-- Instituciones Financieras por Periodo
LEFT JOIN E_DW_VIEWS.V_CONFIG_EMPR_FINANC_VPC_HSTEMPR 
	ON FRRC.Cod_Instit_Financiera = EMPR.CodEmpresaFinanc_Cd
	-- Si es 20991231 toma la ultima fecha disponible, es decir la fecha Ini de ese registro
	AND EMPR.FecInformacion_Dt = CASE WHEN PEEM.FechaFin = '2099-12-31' THEN (PEEM.FechaIni-1) ELSE PEEM.FechaFin END
-- DIM Tipo Empresa
LEFT JOIN E_DW_VIEWS.V_DIM_ESTADO_EMPR_FINANC_VPC DEEM
	ON EMPR.EstadoEmpresaFinanc_Id = DEEM.EstadoEmpresaFinanc_Id
-- DIM Estado Empresa
LEFT JOIN E_DW_VIEWS.V_DIM_TIPO_EMPR_FINANC_VPC DTEM
	ON EMPR.TipoEmpresaFinanc_Id = DTEM.TipoEmpresaFinanc_Id
-- DIM Tipo de Credito
LEFT JOIN E_DW_VIEWS.V_RSK_RCC_DIM_TIPO_CREDITO DTCR
	ON FRRC.Tipo_Credito = DTCR.RTCR_CodTipoCred
-- Party_SBS
LEFT JOIN (
	SELECT 
		Party_Id
		,Party_Identification_Num
	FROM E_DW_VIEWS.PARTY
	WHERE Party_Identification_Type_Cd = 12 --Codigo SBS	
) PSBS
	ON FRRC.Codigo_SBS = PSBS.Party_Identification_Num	
-- Party Persona
LEFT JOIN (
	SELECT
		TPAR.Party_Id
		,TPAR.Party_Identification_Type_Cd
		,TPIT.Party_Identificat_Type_Host_Cd
		,TPIT.Party_Identification_Type_Desc
		,TPAR.Party_Identification_Num
		,ENDH.Key_Value
	FROM E_DW_VIEWS.PARTY TPAR
	-- Tipo de Identificacion
	LEFT JOIN E_DW_VIEWS.PARTY_IDENTIFICATION_TYPE TPIT
		ON TPAR.Party_Identification_Type_Cd = TPIT.Party_Identification_Type_Cd
	--Encriptacion	
	LEFT JOIN E_DW_VIEWS.V_MST_ENCRIP_NRO_DOC_MDL_HISTDIA ENDH
		ON TPAR.Party_Identification_Num = ENDH.Nro_Doc
		-- Particion minima en tabla de encriptacion
		AND ENDH.CodDia BETWEEN '20160101'(DATE, FORMAT 'yyyymmdd') AND CURRENT_DATE		
) PPER
	--Party_Persona
	ON FRRC.Cod_Tipo_Documento = PPER.Party_Identificat_Type_Host_Cd
	AND FRRC.Nro_Documento = PPER.Party_Identification_Num
-- Tipo de Identificacion
LEFT JOIN E_DW_VIEWS.PARTY_IDENTIFICATION_TYPE PITY
	ON SMRC.Cod_Tipo_Documento = PITY.Party_Identificat_Type_Host_Cd
-- Cliente
LEFT JOIN (
	SELECT 
		Cliente_Id
		,Persona_Id
		,Tipo_Identificacion_Host
		,Tipo_Identificacion_Desc
		,Numero_Identificacion
		,CUC_Num 
	FROM E_DW_VIEWS.V_DIM_CLIENTE 
	QUALIFY Row_Number() Over(PARTITION BY Tipo_Identificacion_Host, Numero_Identificacion ORDER BY Fecha_Creacion DESC) = 1
) DICL
	ON FRRC.Cod_Tipo_Documento = DICL.Tipo_Identificacion_Host 
	AND FRRC.Nro_Documento = DICL.Numero_Identificacion
-- Tipo de Producto RCC
LEFT JOIN (
	SELECT 
		Parameter_Value
		,1 AS FLG 
	FROM E_DW_VIEWS.V_PARAMETERS 
	WHERE Parameter_Table_Name = 'REL_VPC_SEGMENTACION_MERC' 
		AND Parameter_Name = 'TIPO_PRODUCTO_RCC' 
		AND Parameter_Group_Desc = 'ACTIVO'     
) TPRC
	ON CTRC.Tipo_Producto_RCC_Desc = TPRC.Parameter_Value
-- Codigo Estatus Empresa
LEFT JOIN (
	SELECT 
		Parameter_Value
		, 1 AS FLG 
	FROM E_DW_VIEWS.V_PARAMETERS 
	WHERE Parameter_Table_Name = 'REL_VPC_SEGMENTACION_MERC' 
    	AND Parameter_Name = 'COD_STATUS_EMPRESA' 
		AND Parameter_Group_Desc = 'ACTIVO'   
) CEMP
	ON EMPR.EstadoEmpresaFinanc_Id = CEMP.Parameter_Value
-- Tipo Empresa
LEFT JOIN (
	SELECT 
		Parameter_Value
		,1 AS FLG 
	FROM E_DW_VIEWS.V_PARAMETERS 
	WHERE Parameter_Table_Name = 'REL_VPC_SEGMENTACION_MERC' 
		AND Parameter_Name = 'TIP_EMPRESA' 
		AND Parameter_Group_Desc = 'ACTIVO'
) TIEM
	ON EMPR.TipoEmpresaFinanc_Id = TIEM.Parameter_Value
-- Empresa No Financiera
LEFT JOIN (
	SELECT 
		Parameter_Value
		,1 AS FLG 
	FROM E_DW_VIEWS.V_PARAMETERS 
	WHERE Parameter_Table_Name = 'REL_VPC_SEGMENTACION_MERC' 
		AND Parameter_Name = 'NO_EMPRESA_FINAN' 
		AND Parameter_Group_Desc = 'ACTIVO'        
) ENNF 
	ON EMPR.CodEmpresaFinanc_Cd  = ENNF.Parameter_Value
-- Solo aquellas empresas que sigan el Tipo de Credito paramerizado
WHERE TRIM(FRRC.Tipo_Credito) IN 
(
    SELECT 
		Parameter_Value 
	FROM E_DW_VIEWS.V_PARAMETERS 
	WHERE Parameter_Table_Name = 'REL_VPC_SEGMENTACION_MERC' 
		AND Parameter_Name = 'TIPO_CREDITO_RCC' 
		AND Parameter_Group_Desc = 'ACTIVO'     
);